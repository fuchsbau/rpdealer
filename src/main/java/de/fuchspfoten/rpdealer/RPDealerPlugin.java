package de.fuchspfoten.rpdealer;

import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent.Status;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;

/**
 * Main class for the plugin.
 */
public class RPDealerPlugin extends JavaPlugin implements Listener {

    /**
     * The source URI.
     */
    private String rpSource;

    /**
     * The resource pack hash.
     */
    private byte[] rpHash;

    /**
     * The object on which accesses to the hash lock.
     */
    private final Object lock = new Object();

    /**
     * The set of UUIDs without the resource pack.
     */
    private final Collection<UUID> noResourcePack = new HashSet<>();

    @EventHandler
    public void onPlayerLogin(final PlayerLoginEvent event) {
        synchronized (lock) {
            if (rpHash == null) {
                event.disallow(Result.KICK_OTHER, "Server is not ready yet.");
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final byte[] hash;
        synchronized (lock) {
            hash = rpHash.clone();
        }

        final Player joiner = event.getPlayer();
        getServer().getScheduler().scheduleSyncDelayedTask(this, () -> {
            noResourcePack.add(joiner.getUniqueId());
            event.getPlayer().setResourcePack(rpSource, hash);
            getLogger().info(joiner.getName() + " is asked to load the resource pack.");
        }, 100L);
    }

    @EventHandler
    public void onPlayerResourcePackStatus(final PlayerResourcePackStatusEvent event) {
        if (event.getStatus() == Status.SUCCESSFULLY_LOADED) {
            noResourcePack.remove(event.getPlayer().getUniqueId());
            getLogger().info(event.getPlayer().getName() + " loaded the resource pack!");
        } else {
            getLogger().info(event.getPlayer().getName() + " resource pack status: " + event.getStatus().name());
        }
    }

    /**
     * Fetches the SHA1 hash from the server.
     */
    private void fetchHash() {
        try {
            final URL url = new URL(rpSource + ".sha1");
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            final String textHash;
            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                final StringBuilder buffer = new StringBuilder();
                while (true) {
                    final String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    buffer.append(line);
                }
                textHash = buffer.toString().trim();
            }
            connection.disconnect();

            // Store the hash.
            getLogger().info("Found hash: " + textHash);

            // Check the hash.
            if (textHash.length() != 40) {
                throw new IllegalStateException("Invalid hash " + textHash);
            }

            // Convert the hash.
            synchronized (lock) {
                rpHash = new byte[20];
                for (int i = 0; i < 20; i++) {
                    final short uByte = Short.parseShort(textHash.substring(i * 2, i * 2 + 2), 16);
                    rpHash[i] = (byte) (uByte & 0xFF);
                }
            }
        } catch (final IOException e) {
            throw new IllegalStateException("Could not load sha1 hash!", e);
        }
    }

    @Override
    public void onEnable() {
        // Setup the config.
        getConfig().options().copyDefaults(true);
        saveConfig();
        rpSource = getConfig().getString("source");

        // Register messages.
        Messenger.register("rpdealer.notusing");

        // Fetch the hash asynchronously.
        getLogger().info("Fetching SHA1 hash of RP!");
        getServer().getScheduler().runTaskAsynchronously(this, () -> {
            fetchHash();
            getLogger().info("Hash of resource pack found");
        });

        // Register listeners.
        getServer().getPluginManager().registerEvents(this, this);

        // Initialize the warning message.
        getServer().getScheduler().scheduleSyncRepeatingTask(this,
                () -> noResourcePack.stream()
                        .map(Bukkit::getPlayer)
                        .filter(Objects::nonNull)
                        .filter(Player::isOnline)
                        .forEach(p -> Messenger.send(p, "rpdealer.notusing")), 20L * 60, 20L * 300);
    }
}
